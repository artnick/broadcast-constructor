import { expect } from 'chai'
import { addTextMessage, addButton, updateTextMessage } from '../src/core'



describe('application logic', () => {

  describe('addTextMessage', () => {

    it('add message', () => {
    	const initialState = []
			const action = {
				id: 0,
				path: []
			};
      const nextState = addTextMessage(initialState, action);
      expect(nextState).to.eql([
					{
			      id: 0,
			      type: 'text',
			      text: '',
			      buttons: []
			  	}
				]
			);
    });

    it('add message in 2 level', () => {
    	const initialState = [
				{
		      id: 0,
		      type: 'text',
		      text: '',
		      buttons: [
		      	{
		      		id: 1,
              type: 'message',
              title: 'Новая кнопка',
		        	messages: []
		  			}
		  		]
		  	}
			]
			const action = {
				id: 2,
				targetId: 1,
				path: [1]
			};
      const nextState = addTextMessage(initialState, action);
      expect(nextState).to.eql(
      	[
					{
			      id: 0,
			      type: 'text',
			      text: '',
			      buttons: [
			      	{
			      		id: 1,
	              type: 'message',
	              title: 'Новая кнопка',
			        	messages: [
			        		{
							      id: 2,
							      type: 'text',
							      text: '',
							      buttons: []
						  		}
					  		]
			  			}
			  		]
			  	}
				]
			);
    });

    it('add message in 3 level', () => {
    	const initialState = [
				{
		      id: 0,
		      type: 'text',
		      text: '',
		      buttons: [
		      	{
		      		id: 1,
              type: 'message',
              title: 'Новая кнопка',
		        	messages: [
		        		{
						      id: 2,
						      type: 'text',
						      text: '',
						      buttons: [
					      		{
						      		id: 3,
				              type: 'message',
				              title: 'Новая кнопка',
						        	messages: []
					  				}
						      ]
					  		}
				  		]
		  			}
		  		]
		  	}
			]
			const action = {
				id: 4,
				path: [1,3]
			};
      const nextState = addTextMessage(initialState, action);
      expect(nextState).to.eql(
      	[
					{
			      id: 0,
			      type: 'text',
			      text: '',
			      buttons: [
			      	{
			      		id: 1,
	              type: 'message',
	              title: 'Новая кнопка',
			        	messages: [
			        		{
							      id: 2,
							      type: 'text',
							      text: '',
							      buttons: [
							      	{
							      		id: 3,
					              type: 'message',
					              title: 'Новая кнопка',
							        	messages: [
							        		{
											      id: 4,
											      type: 'text',
											      text: '',
											      buttons: []
										  		}
									  		]
						  				}
						  			]
						  		}
					  		]
			  			}
			  		]
			  	}
				]
			);
    });

    it('add second message in 2 level', () => {
    	const initialState = [
				{
		      id: 0,
		      type: 'text',
		      text: '',
		      buttons: [
		      	{
		      		id: 1,
              type: 'message',
              title: 'Новая кнопка',
		        	messages: [
		        		{
						      id: 2,
						      type: 'text',
						      text: '',
						      buttons: []
					  		}
				  		]
		  			}
		  		]
		  	}
			]
			const action = {
				id: 4,
				path: [1]
			};
      const nextState = addTextMessage(initialState, action);
      expect(nextState).to.eql(
      	[
					{
			      id: 0,
			      type: 'text',
			      text: '',
			      buttons: [
			      	{
			      		id: 1,
	              type: 'message',
	              title: 'Новая кнопка',
			        	messages: [
			        		{
							      id: 2,
							      type: 'text',
							      text: '',
							      buttons: []
						  		},
						  		{
						  			id: 4,
							      type: 'text',
							      text: '',
							      buttons: []
						  		}
					  		]
			  			}
			  		]
			  	}
				]
			);
    });

  });

  describe('updateTextMessage', () => {

    it('update text', () => {

    	const initialState = [
				{
		      id: 0,
		      type: 'text',
		      text: '',
		      buttons: []
		  	},
				{
		      id: 1,
		      type: 'text',
		      text: '',
		      buttons: []
		  	}
			]

			const action = {
				id: 1,
				text: 'new',
				path: []
			};

      const nextState =  updateTextMessage(initialState, action);
      expect(nextState).to.eql([
				{
		      id: 0,
		      type: 'text',
		      text: '',
		      buttons: []
		  	},
				{
		      id: 1,
		      type: 'text',
		      text: 'new',
		      buttons: []
		  	}
			]);
    });


  });

  describe('addButton', () => {

    it('add button', () => {

    	const initialState = [
				{
		      id: 0,
		      type: 'text',
		      title: '',
		      buttons: []
		  	}
			]
			const action = {
				id: 1,
				targetId: 0
			};
      const nextState =  addButton(initialState, action);
      expect(nextState).to.eql([
				{
		      id: 0,
		      type: 'text',
		      title: '',
		      buttons: [
		      	{
		      		id: 1,
              type: 'message',
              title: 'Новая кнопка',
		        	messages: []
		  			}
		  		]
		  	}
			]);
    });

    it('add second button', () => {

    	const initialState = [
				{
		      id: 0,
		      type: 'text',
		      title: '',
		      buttons: [
		      	{
		      		id: 1,
              type: 'message',
              title: 'Новая кнопка',
		        	messages: []
		  			}
		  		]
		  	}
			]
			const action = {
				id: 2,
				targetId: 0
			};
      const nextState =  addButton(initialState, action);
      expect(nextState).to.eql([
				{
		      id: 0,
		      type: 'text',
		      title: '',
		      buttons: [
		      	{
		      		id: 1,
              type: 'message',
              title: 'Новая кнопка',
		        	messages: []
		  			},
		  			{
		      		id: 2,
              type: 'message',
              title: 'Новая кнопка',
		        	messages: []
		  			}
		  		]
		  	}
			]);
    });

  });



});