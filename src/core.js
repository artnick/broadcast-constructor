function newTextMessage(id) {
	return {
		id,
		type: 'text',
		text: '',
		buttons: []
	}
}

function newButton(id) {
	return {
		id,
    type: 'message',
    title: 'Новая кнопка',
    messages: []
	}
}

function updateObject(oldObject, newValues) {
    return {...oldObject, ...newValues};
}

function updateItemInArray(array, itemId, updateItemCallback) {
    const updatedItems = array.map(item => {
        if(item.id !== itemId) {
            return item;
        }
        const updatedItem = updateItemCallback(item);
        return updatedItem;
    });
    return updatedItems;
}


export const addTextMessage = (messages, action) => {
	if(action.path.length) {
		const targetId = action.path.shift()
		return messages.map(message => {
			if(message.type === 'text' && message.buttons.length)
				return {
					...message,
					buttons: updateItemInArray(message.buttons, targetId, button => {
						return updateObject(
		      		button,
		      		{
		      			messages: addTextMessage(button.messages, action)
		      		} 
		      	)
					})
				}
			else
				return message
		})
	}
	else
		return messages.concat(newTextMessage(action.id));
}

export const moveMessages = (messages, action) => {
	messages.splice(action.newIndex, 0, messages.splice(action.oldIndex, 1)[0])
	return [...messages]
}

export const updateTextMessage = (messages, action) => {
	return updateItemInArray(messages, action.id, message => {
    	return updateObject(
      	message, 
      	{
      		text: action.text
				}
			);
  });
}

export const addButton = (state, action) => {
	return updateItemInArray(state, action.targetId, item => {
    	return updateObject(
      	item, 
      	{
      		buttons: item.buttons.concat(newButton(action.id))
				}
			);
  });
}