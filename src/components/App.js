import React from 'react'
import Messages from '../containers/Messages'

const App = () => (
  <div>
  	<Messages/>
  </div>
);

export default App;