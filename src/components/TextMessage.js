import React, {Component} from 'react'
import {SortableElement, SortableHandle} from 'react-sortable-hoc';
import styled from 'styled-components'



const DragHandle = SortableHandle(() => <span>::::</span>); 

const TextArea = styled.textarea`
		padding: 5px 16px;
    overflow: hidden;
    font-size: 14px;
    line-height: 1.42857143;
  	height: 15px;
  	border-radius: 0;
    border-color: transparent;
    color: #253237;
    font-weight: 500;
    resize: none;
`;


const TextMessage = SortableElement(({ message, onTextChange }) => {
		const handleTextChange = (e) => {
			onTextChange(message.id, e.target.value)
		}

	  return (
	      <li>
	          <DragHandle />
	          <TextArea 
	          	rows={1} 
	          	placeholder={'Добавить текст'} 
	          	value={message.text} 
	          	onChange={handleTextChange}
	          />
	      </li>
	  );
});

export default TextMessage;