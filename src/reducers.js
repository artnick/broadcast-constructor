import { ADD_TEXT_MESSAGE, MOVE_MESSAGES, UPDATE_TEXT_MESSAGE} from './actions'
import { addTextMessage, moveMessages, updateTextMessage} from './core'
import { combineReducers } from 'redux'

const initialState = [
  {
    id: 0,
    type: 'text',
    text: 'Сообщение 1',
    buttons: []
  }
]

function messages(state = initialState, action) {
  switch (action.type) {
    case ADD_TEXT_MESSAGE:
      return addTextMessage(state, action);
    case MOVE_MESSAGES:
      return moveMessages(state, action);
    case UPDATE_TEXT_MESSAGE:
      return updateTextMessage(state, action);
    default:
      return state;
  }
}

function path(state = [], action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default combineReducers({
  messages,
  path
})