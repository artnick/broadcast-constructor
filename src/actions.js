/*
 * action types
 */

export const ADD_TEXT_MESSAGE = 'ADD_TEXT_MESSAGE'
export const MOVE_MESSAGES = 'MOVE_MESSAGES'
export const UPDATE_TEXT_MESSAGE = 'UPDATE_TEXT_MESSAGE'

/*
 * action creators
 */
let nextId = 2;

export function addTextMessage() {
  return { 
  	type: ADD_TEXT_MESSAGE, 
  	path: [],  
  	id: nextId++
  }
}

export function moveMessages(oldIndex, newIndex) {
  return { 
  	type: MOVE_MESSAGES,
  	path: [],
  	oldIndex,
  	newIndex
  }
}

export function updateTextMessage(id, text) {
  return { 
  	type: UPDATE_TEXT_MESSAGE,
  	path: [],
  	id,
  	text
  }
}