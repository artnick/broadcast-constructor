import React, {Component} from 'react'
import { connect } from 'react-redux'
import {SortableContainer} from 'react-sortable-hoc';
import styled from 'styled-components'

import { addTextMessage, moveMessages, updateTextMessage } from '../actions'
import TextMessage from '../components/TextMessage'


const AddButton = styled.button`
  background-color: #fff;
  border: 1px solid hsla(0,0%,53%,.8);
  color: #888;
  font-size: 28px;
  padding: 0;
  border-radius: 50%;
  line-height: 50px;
  width: 50px;
  height: 50px;

  &:focus {
    outline: none;
  }
`;

const MessageList = SortableContainer(({items, onTextChange}) => {
	return (
		<ul>
			{items.map((item, index) =>
          <TextMessage key={`item-${index}`} index={index} message={item} onTextChange={onTextChange}/>
      )}
		</ul>
	);
});

const SortableComponent = ({ messages, onAddMessage, onSortEnd, onTextChange}) => {
  return <div>
      <MessageList items={messages} onSortEnd={onSortEnd} useDragHandle={true} onTextChange={onTextChange}/>
      <AddButton onClick={() => onAddMessage()}>+</AddButton> 
    </div>
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAddMessage: () => {
      dispatch(addTextMessage())
    },
    onSortEnd: ({oldIndex, newIndex}) => {
    	dispatch(moveMessages(oldIndex, newIndex))
    },
    onTextChange: (id, text) => {
    	dispatch(updateTextMessage(id, text));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SortableComponent)